# Slovak translation for bijiben.
# Copyright (C) 2013 bijiben's COPYRIGHT HOLDER
# This file is distributed under the same license as the bijiben package.
# Dušan Kazik <prescott66@gmail.com>, 2013.
# Peter Vágner <pvagner@pvagner.tk>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: bijiben gnome-3-8\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-notes/issues\n"
"POT-Creation-Date: 2020-07-22 13:04+0000\n"
"PO-Revision-Date: 2020-09-02 12:44+0200\n"
"Last-Translator: Dušan Kazik <prescott66@gmail.com>\n"
"Language-Team: slovenčina <>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0;\n"
"X-Generator: Poedit 2.4.1\n"

#: data/appdata/org.gnome.Notes.appdata.xml.in:6
#: data/org.gnome.Notes.desktop.in:3 data/org.gnome.Notes.xml.in:4
#: src/bjb-application.c:608 src/bjb-window-base.c:17
msgid "Notes"
msgstr "Poznámky"

#: data/appdata/org.gnome.Notes.appdata.xml.in:7
msgid "Notes for GNOME"
msgstr "Poznámky pre prostredie GNOME"

#: data/appdata/org.gnome.Notes.appdata.xml.in:9
msgid ""
"A quick and easy way to make freeform notes or jot down simple lists. Store "
"as many notes as you like and share them by email."
msgstr ""
"Rýchly a jednoduchý spôsob ako si zapísať poznámky alebo poznačiť zoznamy. "
"Uložiť je možné neobmedzené množstvo poznámok, ktoré je ďalej možné zdieľať "
"emailom."

#: data/appdata/org.gnome.Notes.appdata.xml.in:12
msgid ""
"You can store your notes locally on your computer or sync with online "
"services like ownCloud."
msgstr ""
"Poznámky si môžete uložiť v počítači, alebo ich môžete synchronizovať so "
"službami ako je nextCloud."

#: data/appdata/org.gnome.Notes.appdata.xml.in:28
msgid "Edit view"
msgstr "Zobrazenie úprav"

#: data/appdata/org.gnome.Notes.appdata.xml.in:32
msgid "Select view"
msgstr "Zobrazenie výberu"

#: data/appdata/org.gnome.Notes.appdata.xml.in:36
msgid "List view"
msgstr "Zobrazenie zoznamu"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Notes.desktop.in:5
msgid "notes;reminder;notebook;sticky notes;"
msgstr "poznámky;pripomienka;pripomienky;zápisník;notebook;lepkavé poznámky;"

#: data/org.gnome.Notes.desktop.in:6
msgid "Post notes, tag files!"
msgstr "Zapíšte si poznámky, označte súbory!"

#: data/org.gnome.Notes.desktop.in:7
msgid "Note-taker"
msgstr "Záznam poznámok"

#: data/org.gnome.Notes.desktop.in:19
msgid "Create New Note"
msgstr "Vytvoriť novú poznámku"

#: data/org.gnome.Notes.gschema.xml:10
msgid "Custom Font"
msgstr "Vlastné písmo"

#: data/org.gnome.Notes.gschema.xml:11
msgid "The font name set here will be used as the font when displaying notes."
msgstr ""
"Názov písma, ktorý je tu nastavený, sa použije ako písmo zobrazujúce "
"poznámky."

#: data/org.gnome.Notes.gschema.xml:15
msgid "Whether to use the system monospace font"
msgstr "Či sa má použiť systémové písmo typu monospace"

# label
#: data/org.gnome.Notes.gschema.xml:19
msgid "New notes color."
msgstr "Farba nových poznámok."

#: data/org.gnome.Notes.gschema.xml:20
msgid ""
"The color name set here will be used as the color when creating new notes."
msgstr ""
"Názov farby, ktorý je tu nastavený, sa použije ako farba pri vytváraní "
"nových poznámok."

#: data/org.gnome.Notes.gschema.xml:24
msgid "Primary notes provider to use for new notes."
msgstr "Hlavný poskytovateľ poznámok pre nové poznámky."

#: data/org.gnome.Notes.gschema.xml:25
msgid "The primary notebook is the place where new notes are created."
msgstr "Hlavný zápisník je miesto, kam sa budú vytvárať nové poznámky."

#: data/org.gnome.Notes.gschema.xml:29
msgid "Window maximized"
msgstr "Maximalizovanie okna"

#: data/org.gnome.Notes.gschema.xml:30
msgid "Window maximized state."
msgstr "Stav maximalizácie okna."

#: data/org.gnome.Notes.gschema.xml:34
msgid "Window size"
msgstr "Veľkosť okna"

#: data/org.gnome.Notes.gschema.xml:35
msgid "Window size (width and height)."
msgstr "Veľkosť okna (šírka a výška)."

#: data/org.gnome.Notes.gschema.xml:39
msgid "Window position"
msgstr "Pozícia okna"

#: data/org.gnome.Notes.gschema.xml:40
msgid "Window position (x and y)."
msgstr "Pozícia okna (súradnice X a Y)"

#: data/org.gnome.Notes.gschema.xml:43
msgid "Text size used by note editor."
msgstr "Veľkosť textu použita v editore poznámok."

#: data/org.gnome.Notes.gschema.xml:44
msgid ""
"There are three text sizes available: small, medium (default) and large."
msgstr ""
"K dispozícii sú tri veľkosti textu: malý text, stredný text (predvolené) a "
"veľký text."

#: data/resources/editor-toolbar.ui:51
msgid "Bold"
msgstr "Tučné"

#: data/resources/editor-toolbar.ui:67
msgid "Italic"
msgstr "Kurzíva"

#: data/resources/editor-toolbar.ui:83
msgid "Strike"
msgstr "Preškrtnuté"

# menu item
# DK: vybratim tejto volby program vytvori odrazky vo forme guliciek - preto odrazky
#: data/resources/editor-toolbar.ui:111
msgid "Bullets"
msgstr "Odrážky"

#: data/resources/editor-toolbar.ui:127
msgid "List"
msgstr "Zoznam"

# tooltip
#: data/resources/editor-toolbar.ui:166
msgid "Copy selection to a new note"
msgstr "Skopíruje výber do novej poznámky"

#: data/resources/empty-results-box.ui:33
msgid "No notes"
msgstr "Žiadne poznámky"

#: data/resources/help-overlay.ui:9
msgctxt "shortcut window"
msgid "General"
msgstr "Všeobecné"

# tooltip
#: data/resources/help-overlay.ui:14
msgctxt "shortcut window"
msgid "Search notes"
msgstr "Vyhľadanie poznámok"

#: data/resources/help-overlay.ui:21
msgctxt "shortcut window"
msgid "New note"
msgstr "Nová poznámka"

#: data/resources/help-overlay.ui:28
msgctxt "shortcut window"
msgid "Close window"
msgstr "Zatvorenie okna"

#: data/resources/help-overlay.ui:35
msgctxt "shortcut window"
msgid "Quit"
msgstr "Ukončenie"

#: data/resources/help-overlay.ui:42
msgctxt "shortcut window"
msgid "Go back"
msgstr "Prejsť späť"

#: data/resources/help-overlay.ui:49
msgctxt "shortcut window"
msgid "Show help"
msgstr "Zobrazenie pomocníka"

#: data/resources/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Open menu"
msgstr "Otvorenie ponuky"

#: data/resources/help-overlay.ui:63
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Klávesové skratky"

#: data/resources/help-overlay.ui:71
msgctxt "shortcut window"
msgid "Selection mode"
msgstr "Režim výberu"

# tooltip
#: data/resources/help-overlay.ui:76
msgctxt "shortcut window"
msgid "Cancel selection mode"
msgstr "Ukončenie režimu výberu"

#: data/resources/help-overlay.ui:83
msgctxt "shortcut window"
msgid "Select all"
msgstr "Výber všetkého"

# dialog title
#: data/resources/help-overlay.ui:91
msgctxt "shortcut window"
msgid "Note edit mode"
msgstr "Režim úpravy poznámky"

#: data/resources/help-overlay.ui:96
msgctxt "shortcut window"
msgid "Open in a new window"
msgstr "Otvorenie v novom okne"

#: data/resources/help-overlay.ui:103
msgctxt "shortcut window"
msgid "Bold"
msgstr "Tučné"

#: data/resources/help-overlay.ui:110
msgctxt "shortcut window"
msgid "Italic"
msgstr "Kurzíva"

#: data/resources/help-overlay.ui:117
msgctxt "shortcut window"
msgid "Strike through"
msgstr "Preškrtnuté"

# menu item
#: data/resources/help-overlay.ui:124
msgctxt "shortcut window"
msgid "Undo"
msgstr "Vrátenie späť"

# menu item
#: data/resources/help-overlay.ui:131
msgctxt "shortcut window"
msgid "Redo"
msgstr "Zopakovanie"

#: data/resources/help-overlay.ui:138
msgctxt "shortcut window"
msgid "Move note to trash"
msgstr "Presunutie do Koša"

# gtk_window_set_title
#: data/resources/import-dialog.ui:5 data/resources/main-toolbar.ui:180
msgid "Import Notes"
msgstr "Import poznámok"

#: data/resources/import-dialog.ui:12 data/resources/main-toolbar.ui:72
msgid "_Cancel"
msgstr "_Zrušiť"

# dialog title
#: data/resources/import-dialog.ui:22
msgid "_Import"
msgstr "_Importovať"

# gtk_label_new
#: data/resources/import-dialog.ui:41
msgid "Select import location"
msgstr "Vyberte umiestnenie importu"

#: data/resources/import-dialog.ui:78
msgid "Gnote application"
msgstr "Aplikácia Gnote"

#: data/resources/import-dialog.ui:136
msgid "Tomboy application"
msgstr "Aplikácia Tomboy"

#: data/resources/import-dialog.ui:192
msgid "Custom Location"
msgstr "Vlastné umiestnenie"

#: data/resources/import-dialog.ui:224
msgid "Select a Folder"
msgstr "Výber priečinka"

#: data/resources/main-toolbar.ui:14
msgid "_New"
msgstr "_Nová"

# label
#: data/resources/main-toolbar.ui:43 data/resources/selection-toolbar.ui:43
msgid "Note color"
msgstr "Farba poznámok"

#. Translators: “Empty” is a verb.
#: data/resources/main-toolbar.ui:55
msgid "_Empty"
msgstr "Vy_prádzniť"

# tooltip
#: data/resources/main-toolbar.ui:74
msgid "Exit selection mode"
msgstr "Ukončí režim výberu"

#: data/resources/main-toolbar.ui:94
msgid "Search note titles, content and notebooks"
msgstr "Hľadajte v názvoch poznámok, obsahu a zápisníkoch"

#: data/resources/main-toolbar.ui:116
msgid "Selection mode"
msgstr "Režim výberu"

#: data/resources/main-toolbar.ui:130
msgid "Open menu"
msgstr "Otvoriť ponuku"

#: data/resources/main-toolbar.ui:150
msgid "More options"
msgstr "Viac volieb"

#: data/resources/main-toolbar.ui:189
msgid "View Trash"
msgstr "Zobraziť Kôš"

#: data/resources/main-toolbar.ui:206
msgid "Text Sizes"
msgstr "Veľkosti textu"

#: data/resources/main-toolbar.ui:216
msgid "_Large"
msgstr "_Veľký"

#: data/resources/main-toolbar.ui:226
msgid "_Medium"
msgstr "_Stredný"

#: data/resources/main-toolbar.ui:236
msgid "_Small"
msgstr "_Malý"

#: data/resources/main-toolbar.ui:254 data/resources/settings-dialog.ui:10
msgid "Preferences"
msgstr "Nastavenia"

#: data/resources/main-toolbar.ui:263
msgid "Keyboard Shortcuts"
msgstr "Klávesové skratky"

#: data/resources/main-toolbar.ui:272
msgid "Help"
msgstr "Pomocník"

# gtk_window_set_title
#: data/resources/main-toolbar.ui:281
msgid "About Notes"
msgstr "O aplikácii Poznámky"

#: data/resources/main-toolbar.ui:303
msgid "Open in New Window"
msgstr "Otvoriť v novom okne"

# menu item
#: data/resources/main-toolbar.ui:320
msgid "Undo"
msgstr "Vrátiť späť"

# menu item
#: data/resources/main-toolbar.ui:329
msgid "Redo"
msgstr "Opakovať vrátené"

#: data/resources/main-toolbar.ui:346 data/resources/organize-dialog.ui:4
#: data/resources/selection-toolbar.ui:34
msgid "Notebooks"
msgstr "Zápisníky"

# menu item
#: data/resources/main-toolbar.ui:355
msgid "Email this Note"
msgstr "Odoslať túto poznámku e-mailom"

#: data/resources/main-toolbar.ui:364 data/resources/selection-toolbar.ui:96
msgid "Move to Trash"
msgstr "Presunúť do Koša"

# label
#: data/resources/organize-dialog.ui:16
msgid "Enter a name to create a notebook"
msgstr "Zadajte názov pre vytvorený zápisník"

#: data/resources/organize-dialog.ui:37
msgid "New notebook"
msgstr "Nový zápisník"

# tooltip
#: data/resources/selection-toolbar.ui:60
msgid "Share note"
msgstr "Sprístupní poznámku"

#: data/resources/selection-toolbar.ui:78
msgid "Open in another window"
msgstr "Otvoriť v inom okne"

#: data/resources/selection-toolbar.ui:119
msgid "Restore"
msgstr "Obnoviť"

#: data/resources/selection-toolbar.ui:127
msgid "Permanently Delete"
msgstr "Trvalo odstrániť"

#: data/resources/settings-dialog.ui:37
msgid "Use System Font"
msgstr "Použiť systémové písmo"

# label
#: data/resources/settings-dialog.ui:50
msgid "Note Font"
msgstr "Písmo poznámok"

# label
#: data/resources/settings-dialog.ui:62
msgid "Default Color"
msgstr "Predvolená farba"

#: data/resources/settings-dialog.ui:113
msgid "Note Appearance"
msgstr "Vzhľad poznámky"

# gtk_label_new
#: data/resources/settings-dialog.ui:126
msgid "Select the default storage location:"
msgstr "Vyberte predvolené umiestnenie úložiska:"

#: data/resources/settings-dialog.ui:157
msgid "Primary Book"
msgstr "Hlavná kniha"

#: src/bijiben-shell-search-provider.c:270 src/bjb-main-toolbar.c:348
msgid "Untitled"
msgstr "Bez názvu"

# cmd desc
#: src/bjb-application.c:432
msgid "Show the application’s version"
msgstr "Vypíše verziu aplikácie"

# cmd desc
#: src/bjb-application.c:434
msgid "Create a new note"
msgstr "Vytvorí novú poznámku"

#: src/bjb-application.c:436
msgid "[FILE…]"
msgstr "[SÚBOR…]"

#: src/bjb-application.c:444
msgid "Take notes and export them everywhere."
msgstr "Umožňuje zápis poznámok a ich export."

#. Translators: this is a fatal error quit message
#. * printed on the command line
#: src/bjb-application.c:455
msgid "Could not parse arguments"
msgstr "Nepodarilo sa spracovať parametre"

#: src/bjb-application.c:463
msgid "GNOME Notes"
msgstr "Poznámky prostredia GNOME"

#. Translators: this is a fatal error quit message
#. * printed on the command line
#: src/bjb-application.c:475
msgid "Could not register the application"
msgstr "Nepodarilo sa zaregistrovať aplikáciu"

#: src/bjb-application.c:609
msgid "Simple notebook for GNOME"
msgstr "Jednoduchý zápisník pre prostredie GNOME"

#: src/bjb-application.c:615
msgid "translator-credits"
msgstr "Dušan Kazik <prescott66@gmail.com>"

# DK:meni farbu danej poznamky
# label
#: src/bjb-color-button.c:140
msgid "Note Color"
msgstr "Farba poznámky"

# label
#: src/bjb-controller.c:239
msgid "Notebook"
msgstr "Zápisník"

# label
#: src/bjb-empty-results-box.c:63
msgid "Press the New button to create a note."
msgstr "Stlačením tlačidla Nová vytvoríte novú poznámku."

# DK: https://bugzilla.gnome.org/show_bug.cgi?id=706476
#: src/bjb-empty-results-box.c:85
msgid "Oops"
msgstr "Ale nie"

#: src/bjb-empty-results-box.c:91
msgid "Please install “Tracker” then restart the application."
msgstr ""
"Prosím, nainštalujte program „Tracker“, a potom znovu spustite aplikáciu."

#: src/bjb-main-toolbar.c:134
msgid "Click on items to select them"
msgstr "Kliknutím na položky ich vyberiete"

#: src/bjb-main-toolbar.c:136
#, c-format
msgid "%d selected"
msgid_plural "%d selected"
msgstr[0] "%d vybraných"
msgstr[1] "%d vybraná"
msgstr[2] "%d vybrané"

#: src/bjb-main-toolbar.c:228
#, c-format
msgid "Results for %s"
msgstr "Výsledky pre %s"

#: src/bjb-main-toolbar.c:231
msgid "New and Recent"
msgstr "Nové a nedávno použité"

#: src/bjb-main-toolbar.c:324
msgid "Trash"
msgstr "Kôš"

#. Translators: %s is the note last recency description.
#. * Last updated is placed as in left to right language
#. * right to left languages might move %s
#. *         '%s Last Updated'
#.
#: src/bjb-main-toolbar.c:392
#, c-format
msgid "Last updated: %s"
msgstr "Naposledy aktualizované %s"

#: src/bjb-utils.c:51 src/libbiji/biji-date-time.c:30
#: src/libbiji/biji-date-time.c:51
msgid "Unknown"
msgstr "Nevedno kedy"

#: src/bjb-utils.c:68 src/libbiji/biji-date-time.c:42
msgid "Yesterday"
msgstr "Včera"

#: src/bjb-utils.c:74 src/libbiji/biji-date-time.c:56
msgid "This month"
msgstr "V tomto mesiaci"

#: src/libbiji/biji-date-time.c:38
msgid "Today"
msgstr "Dnes"

#: src/libbiji/biji-date-time.c:47
msgid "This week"
msgstr "V tomto týždni"

#: src/libbiji/biji-date-time.c:60
msgid "This year"
msgstr "V tomto roku"

#: src/libbiji/biji-notebook.c:265
msgid "Local"
msgstr "Miestny"

#: src/libbiji/provider/biji-local-provider.c:366
msgid "Local storage"
msgstr "Miestne úložisko"

#~ msgid "How to show note items"
#~ msgstr "Ako zobraziť položky poznámok"

#~ msgid "Whether to show note items in icon view or list view."
#~ msgstr ""
#~ "Či sa majú položky poznámok zobraziť v zobrazení ikôn alebo zoznamu."

# tooltip
#~ msgid "View notes and notebooks in a grid"
#~ msgstr "Zobrazí poznámky a zápisníky v mriežke"

# tooltip
#~ msgid "View notes and notebooks in a list"
#~ msgstr "Zobrazí poznámky a zápisníky v zozname"

#~ msgid "Load More"
#~ msgstr "Načítať viac"

#~ msgid "Loading…"
#~ msgstr "Načítava sa…"

#~ msgctxt "shortcut window"
#~ msgid "Shortcuts"
#~ msgstr "Klávesové skratky"

#~ msgid "Bijiben"
#~ msgstr "Bijiben"

#~ msgid "Quickly jot notes"
#~ msgstr "Rýchle zapísanie poznámok"

#~ msgid "org.gnome.bijiben"
#~ msgstr "org.gnome.bijiben"

#~ msgid "_Import Notes"
#~ msgstr "_Importovať poznámky"

#~ msgid "_View Trash"
#~ msgstr "_Zobraziť Kôš"

#~ msgid "_Preferences"
#~ msgstr "Nas_tavenia"

#~ msgid "_Help"
#~ msgstr "_Pomocník"

#~ msgid "_Keyboard Shortcuts"
#~ msgstr "_Klávesové skratky"

#~ msgid "_About"
#~ msgstr "_O programe"

#~ msgid "Cut"
#~ msgstr "Vystrihnúť"

#~ msgid "Copy"
#~ msgstr "Kopírovať"

#~ msgid "Paste"
#~ msgstr "Prilepiť"

#~ msgid "* "
#~ msgstr "* "

#~ msgid "1."
#~ msgstr "1."

#~| msgid "_About"
#~ msgid "About"
#~ msgstr "O programe"

#~ msgid "Notes is an editor allowing to make simple lists for later use."
#~ msgstr ""
#~ "Aplikácia Poznámky je editor, ktorý umožňuje vytvoriť jednoduché zápisky "
#~ "na neskoršie pripomenutie."

#~ msgid "It allows to use ownCloud as a storage location and online editor."
#~ msgstr ""
#~ "Umožňuje použiť službu ownCloud ako miesto uloženia a online editor."

# dialog title
#~ msgid "Note Edition"
#~ msgstr "Úprava poznámky"

#~ msgid "Note"
#~ msgstr "Poznámka"

#~ msgid "notes;reminder;"
#~ msgstr "poznámky;pripomienka;pripomienky;"

#~ msgid "Cancel"
#~ msgstr "Zrušiť"

#~ msgid "_OK"
#~ msgstr "_OK"

#~ msgid "Numbered List"
#~ msgstr "Očíslovaný zoznam"
